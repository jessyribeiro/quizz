﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IniciarQuizz()
    {
        SceneManager.LoadScene("Quizz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void MenuFinal()
    {
        SceneManager.LoadScene("MenuFinal");
    }

    public void ComoJogar()
    {
        SceneManager.LoadScene("ComoJogar");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }

}// FIM CLASS LOGICA

