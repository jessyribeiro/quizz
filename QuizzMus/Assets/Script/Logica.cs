﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quizz
{
    public string texto;
}


public class ClassResposta : Quizz
{
    public bool ecorreta;

    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }

}

public class ClassPergunta : Quizz
{ // INICIO CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    //METODO PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }


    //METODO PARA ADD AS RESPOSTAS
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    //LISTAR OU EXIBIR RESPOSTAS
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    //CHECAR RESPOSTA CORRETA

    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS

//CLASS LÓGICA - PRINCIPAL

public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text TituloPergunta;
    public Button[] respostaBtn = new Button[3];
    public List<ClassPergunta> PerguntasLista = new List<ClassPergunta>();

    private ClassPergunta PerguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int ContagemPerguntas = 0;
    private float tempo;
    private bool CarregarProximaPergunta;
    private int pontos = 0;
    void Start()
    { //METODO START

        ClassPergunta p1 = new ClassPergunta("1. Fui á praia com a Aninha para aprender a ...", 3);
        p1.AddResposta("Surfar", false);
        p1.AddResposta("Nadar", true);
        p1.AddResposta("Dançar", false);
        p1.AddResposta("Cantar", false);

        ClassPergunta p2 = new ClassPergunta("2. Entrei de gaiato no navio, entrei, entrei...", 6);
        p2.AddResposta("por engano", false);
        p2.AddResposta("pela porta", false);
        p2.AddResposta("pelo Cano", true);
        p2.AddResposta("pela janela", false);

        ClassPergunta p3 = new ClassPergunta("3. Nada do que foi será, denovo do jeito que ...", 4);
        p3.AddResposta("já foi um dia", true);
        p3.AddResposta("foi ontem", false);
        p3.AddResposta("era antes", false);
        p3.AddResposta("era para ter sido", false);

        ClassPergunta p4 = new ClassPergunta("4. Não me abandone, não me desespere, que eu não posso ficar ...", 2);
        p4.AddResposta("com você", false);
        p4.AddResposta("em casa", false);
        p4.AddResposta("viajando", false);
        p4.AddResposta("sem você", true);

        ClassPergunta p5 = new ClassPergunta("5. Eu queria ser um peixe para seu lindo aquário ...", 2);
        p5.AddResposta("Me afogar",false);
        p5.AddResposta("Nadar", false);
        p5.AddResposta("Navegar", false);
        p5.AddResposta("Mergulhar", true);

        ClassPergunta p6 = new ClassPergunta("6. Há uma estrela solta pelo céu da boca se alguém diz ...", 5);
        p6.AddResposta("te amo", true);
        p6.AddResposta("te quero", false);
        p6.AddResposta("estou aqui", false);
        p6.AddResposta("fique comigo", false);

        ClassPergunta p7 = new ClassPergunta("7. 00h no meu quarto, ela vai surgir, ouço passos na escada, eu vejo ...", 6);
        p7.AddResposta("a porta fechar", false);
        p7.AddResposta("a porta abrir", true);
        p7.AddResposta("as janelas baterem", false);
        p7.AddResposta("ela entrar", false);

        ClassPergunta p8 = new ClassPergunta("8. Tira a calça jeans, boa o fio dental, morena você é tão ...", 4);
        p8.AddResposta("Sexy", false);
        p8.AddResposta("Linda", false);
        p8.AddResposta("Sensual", true);
        p8.AddResposta("Gordinha", false);

        ClassPergunta p9 = new ClassPergunta("9. Eu fui numa festa, gelo e cuba - libre.E na vitrola...",5);
        p9.AddResposta("Wisky com gelo", false);
        p9.AddResposta("Vodka e Wisky", false);
        p9.AddResposta("Wisky a go-go", true);
        p9.AddResposta("Vodka a gogo", false);

        ClassPergunta p10 = new ClassPergunta("10. Você é luz, é raio estre e luar, manhã de sol ...", 3);
        p10.AddResposta("meu iaiá, meu ioiô", true);
        p10.AddResposta("meu bombom de coco", false);
        p10.AddResposta("meu amor", false);
        p10.AddResposta("meu pão de mel", false);

        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        PerguntaAtual = p1;
        ExibirPerguntasNoQuizz();

    } // Fim Método Start

    // UPDATE
    void Update()
    {
        if (CarregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                CarregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }
    private void ExibirPerguntasNoQuizz()
    {
        TituloPergunta.text = PerguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = PerguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = PerguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = PerguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = PerguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        ContagemPerguntas++;
        if (ContagemPerguntas < PerguntasLista.Count)
        {
            PerguntaAtual = PerguntasLista[ContagemPerguntas];
            ExibirPerguntasNoQuizz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos" + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (PerguntaAtual.respostas[pos].ecorreta)
        {
            PainelCorreto.SetActive(true);
            pontos += PerguntaAtual.pontos;
        }
        else
        {
            PainelErrado.SetActive(true);
        }

        CarregarProximaPergunta = true;

    }

    public void IniciarQuizz()
    {
        SceneManager.LoadScene("Quizz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void MenuFinal()
    {
        SceneManager.LoadScene("MenuFinal");
    }

    public void ComoJogar()
    {
        SceneManager.LoadScene("ComoJogar");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }

}// FIM CLASS LOGICA

